# Revision Control
## GitLab.com

## Author: Mark W Anderson

-------------

# Table of Contents

## Chapters
###1. What is revision Control?
###2. Initial Organization
###3. Initial Setup on GitLab

---------------
# What is version Control

Version control is a tool that allow you to incrementally save your work with comments. The comments and time stamps make it easy to find a specific version. You can then revert back to a previous version if you ever need to. Additionally I will often check out code at a specific point for another project. This can be done entirely on a single machine and actually can be done by creating directories and copying all work into them on a regular basis. I do not recommend this approach and will cover other approaches and the pros and cons of them.

What is wrong with the directory naming and copying method? Nothing if it your only option but you are missing out on a lot of great features and space saving techniques. To me the greatest feature you are missing out on is remote storage or backup light. Almost all current version control systems allow you to access them remotely and from multiple sources. To this end even if you work alone your data can be backed up in a remote location and you can access it from more than one system.

If you create many versions of your work and check it in frequently, which I recommend, you can use a lot of disk space. Especially if you have image files and other assets that do not change between versions. Modern revision control systems only record the differences where they can. For some digital assets like images they may store the entire binary when a new version is created. This also makes sharing you work a lot more practical so you can not only share the current version but the entire life cycle of the files with a minimal size.

The next feature you will be missing out on is easy comparisons of versions with all changes listed and easy to identify. This is my most used feature when working with others so I can identify changes. Even when working by myself I use it when I start a project after a break. For long breaks I'll check it in but sometimes I get interrupted and want to see what I have changed to refresh my mind on what I was working on. 

The last item I want to cover is collaboration. As time goes on I find it's often important to have collaboration. Collaboration is a key component of all modern version control and allows you to do many things. It will merge code where it feels it can if multiple version of a file exist. If it does not feel it can merge the version it will ask you for help and you can merge the files into a new version. You may never need this if you segment duties but if needed it's great.  You can also have personal working copies you can send back to the master version. This is great to try a few different approaches some times and then only send the chosen version back to the main files. With version control you can not only track changes but identify who made them. This is great if you have questions on something or need clarification. 

Simplistically version control is a tool for tracking file versions. Current version control tools have a lot of added capabilities and features and I'll be covering enough options to get you start. I have chosen to use GitLab over GitHub. If you are planning on doing open source development you may find that the GitHub is a better fit. I'll also cover a few of the additional features offered by GitLab that are not actually part of the base version control system.

# Initial Organization

If you have ever inherited a lot of digital asses from someone and you could find things based on the organization you are lucky. Although I'm often exited to get started I find it valuable to plan our the structure of different assets before getting started. I would do research based on the project you will have. Use words like organization and best practices. I'll cover some general guidelines hear and features that of version control that may make a difference. 

Security is a common item that isn't thought of up front. Let's say you have assets for several clients and you have a single directory under version control with sub directories per client. You can not at this point allow your clients access to just their assest without allow acces to others. Another example is you are writing several books/articals and my want to share some with a specific editor and not others. To keep it simple I suggest breaking things out by project, audience or customer. 

The name of where you store items needs to be descriptive. Project 1 is not a good name for a storage location but may be fine for a subdirectory. If you will have multiple projects under one storage locaiton name the location with a name that describes the reason for the sub projects. This can be clients, leaning a new language, collection of short books about... You will be glad in a year or two when you are looking back over things to use intent and general terms when naming multi use storage locations.

Re-usablility is often not thought out well when implementing smaller projects and can make reuse a lot more difficult than it needs to be. If you will be working on items that may be reused and that are not entirerly containd within a singe file use directories. Almost every repository will have a overall/master set of resources but often you will have sub components for section you want to re-use. Think about what items you may want to reuse and how you can package them to make the self contained. 

If you keep these items in mind you have a good starting place. Focus on best layout practices for whatever type items your will be using version control on. I can't help a lot in this regard due to the sheer number of items people use version control for. Now think up an appropriate name for your initial repository.

# Initial Setup

 Before we actually sign up I need to impress my strong beliefe you need to use a passwords manager. Do not use the same password for more than one site/use and create strong passowrds for all systems. With that said I'll write anothe book on that subject. If you don't have one and are interested I currently use [ZOHO Vault](https://vault.zoho.com/) and am happy with it.
 
 Now on to actually signing up for your GitLab account. You can use alternate methods 
 
 remarkable
 